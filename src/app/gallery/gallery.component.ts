import {Component} from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent{
  passworld = 'asdf';
  title = '';
  url = '';
  shouForm = false;
  img = [
    {title: 'img-1', url: 'https://media-cdn.tripadvisor.com/media/photo-s/0c/bb/a3/97/predator-ride-in-the.jpg'},
    {title: 'img-2', url: 'https://media-cdn.tripadvisor.com/media/photo-s/0c/bb/a3/97/predator-ride-in-the.jpg'},
    {title: 'img-3', url: 'https://media-cdn.tripadvisor.com/media/photo-s/0c/bb/a3/97/predator-ride-in-the.jpg'},
  ];

  onImg(event: Event) {
    event.preventDefault();
    this.img.push({
      title: this.title,
      url: this.url
    })
  }

  onTitle(event: Event) {
    const target = <HTMLInputElement> event.target;
    this.title = target.value;
  }

  onUrl(event: Event) {
    const target = <HTMLInputElement> event.target;
    this.url = target.value;
  }



  onPassworld(event: Event) {
    const target = <HTMLInputElement> event.target;
    if (this.passworld === target.value){
        this.shouForm = true;
    }

  }

}
