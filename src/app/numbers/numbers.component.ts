import { Component } from '@angular/core';

@Component({
  selector: 'app-numbers',
  templateUrl: './numbers.component.html',
  styleUrls: ['./numbers.component.css']
})
export class NumbersComponent {
  numbers: number[] = [];

  constructor() {
    this.numbers = this.getRandom();
    console.log(this.numbers)
  }

  getRandom() {
    const num: number[] = [];
    for (let i = 0; i < 5; i++) {
     let random = Math.floor(Math.random() * (32 - 5 )) + 1;
      if(num.indexOf(random) === -1){
        num.push(random);
      } else {
        let random = Math.floor(Math.random() * (32 - 5 )) + 1;
        num.push(random);
      }
    }
    return num.sort((a, b ) => a > b ? 1 : -1);
  };

  getNewNumbers() {
    this.numbers = this.getRandom();
  }

}
